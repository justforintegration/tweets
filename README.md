# Tweets

Stream tweets to your CLI

## Installation

``go get gitlab.com/hackebrot/tweets``

## Streaming API

Connecting to the [Twitter Statuses Stream][stream] requires authentication via **OAuth1**.

Please [generate tokens][tokens] (read-only access level) and set the following environment variables:

- ``TWEETS_CONSUMER_KEY`` - your Consumer Key (API Key)
- ``TWEETS_CONSUMER_SECRET`` - your Consumer Secret (API Secret)
- ``TWEETS_ACCESS_TOKEN`` - your Access Token
- ``TWEETS_ACCESS_SECRET`` - your Access Token Secret

## Usage

With auth environment variables in place you can now run **tweets**.

To stream tweets related to ``Golang`` run the following:

    $ tweets -track Golang


For tweets related to ``Golang`` or ``gophercon`` separate the keywords with a comma:

    $ tweets -track "Golang,gophercon"


If you want to limit your search to tweets that contain ``#golang``, ``twitter`` and ``API`` use a space:

    $ tweets -track "#golang twitter API"


And finally to track tweets that match either both ``#FOSS`` and ``community``, or both ``golang`` and ``twitter``:

    $ tweets -track "#FOSS community,golang twitter"


You can find out more about about **track** semantics in the official [API docs][track].

## Format

- **blue bold** for the twitter handle of the author, e.g. ``@hackebrot``
- **yellow** for hashtags, e.g. ``#golang``
- **underline** for URLs, e.g. ``https://gitlab.com/hackebrot/tweets``
- **green** for mentioned users, e.g. ``@francesc``
- **default color** for a tweet's text, e.g. ``Stream tweets to your CLI``


# just for func

Tweets made an appearance on [episode #4][justforfunc] of **just for func**, a
YouTube series about Go by [Francesc Campoy][gocampoy].

Francesc reviewed and refactored the source code to be more idiomatic and
explains his thoughts while modifying the code.


[justforfunc]: https://youtu.be/MnbMWNR_XZc
[gocampoy]: https://twitter.com/francesc
[stream]: https://dev.twitter.com/streaming/reference/post/statuses/filter
[tokens]: https://dev.twitter.com/oauth/overview/application-owner-access-tokens
[track]: https://dev.twitter.com/streaming/overview/request-parameters#track
